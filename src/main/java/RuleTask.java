

import java.sql.*;
import java.io.*;
import java.util.*;

class RuleTask extends Thread {
    private Connection con;
    private String idNumber;
    private static String theName;
    private static String evaluator;
    private int conStatus;
    private ruleEngineDriver pool;
    private Vector keys = new Vector(4000);

    public RuleTask(ruleEngineDriver thePool, String evaluator, Vector keys, String theName) {
        conStatus = -5;
        pool = thePool;
        this.idNumber = idNumber;
        this.evaluator = evaluator;
        this.keys = keys;
    }

    public int log(String cmd) {
        java.util.Date now = new java.util.Date();
        System.out.println(now.toString() + " - " + cmd);
        System.out.flush();
        return 0;
    }

    public static String getPassword(String filename) {
        StringBuffer dataResultBuffer = new StringBuffer("");
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            String line;
            while ((line = in.readLine()) != null) {
                dataResultBuffer.append(line);
            }
        } catch (java.io.IOException e) {
            System.out.println(e);
            System.err.println(e);
            System.exit(-1);
        }
        String data = new String(dataResultBuffer);
        return data;
    }

    public int login() {
        String dbHostname = System.getProperty("dbhost");
        String dbSID = System.getProperty("dbsid");
        String dbPort = System.getProperty("dbport");
        String userid = System.getProperty("userid");
        String password = null;
        String dbURL = System.getProperty("dburl");
        String passwordFile = System.getProperty("passwordFile");
        if (passwordFile == null)
            passwordFile = new String("/home/orapass/" + userid);
        if (conStatus == 0) {
            System.out.println(theName + " ALREADY Logged in");
            logout();
        }
        if (dbPort == null) dbPort = new String("1521");
        String connectString = new String("jdbc:oracle:thin:" + userid + "/" + password + "@" + dbHostname + ":" + dbPort + " :" + dbSID);
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        } catch (Exception e) {
            e.printStackTrace();
            conStatus = -1;
            return -1;
        }
        try {
            if (dbURL == null)
                con = DriverManager.getConnection(connectString);
            else
                con = DriverManager.getConnection(dbURL, userid, password);
        } catch (SQLException e) {
            System.err.println("Error : " + connectString);
            e.printStackTrace();
            conStatus = -2;
            return -2;
        }
        conStatus = 0;
        log("Logged in");
        Thread me = Thread.currentThread();
        runSQLCommands("dbms_application_info.set_client_info('engine.RuleTask " + me.getName() + " " + System.getProperty("sourcetable") + "')\n");
        return 0;
    }

    public void logout() {
        try {
            con.close();
            conStatus = -5;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String replaceParms(String command, Vector parms) {
        String newExecutable = new String("");
        int firstParamOffset = command.indexOf('(');
        int LstParmOffset = command.indexOf(')');
        int commas = 0;
        command = command.replace('\'', ' ');
        for (int i = 0; i < command.length(); i++)
            if (command.charAt(i) == ',')
                commas++;
        newExecutable = command.substring(0, firstParamOffset) + "(";
        StringTokenizer st = new StringTokenizer(command.substring(firstParamOffset + 1, LstParmOffset), ",");
        while (st.hasMoreElements()) {
            String element = st.nextToken();
            element = element.trim();
            if (element.length() == 0)
                element = new String(" ");
            parms.addElement(element);
        }
        if (commas > 0) {
            for (int i = 0; i < commas; i++)
                newExecutable = newExecutable + "?,";
            newExecutable = newExecutable + "?)";
        } else newExecutable = newExecutable + "?)";
        return newExecutable;
    }

    public synchronized void runParmCommand(String command) {
        Vector parms = new Vector(20);
        String newCommand = replaceParms(command, parms);
        try {
            CallableStatement call = con.prepareCall("{call " + newCommand + "}");
            for (int i = 0; i < parms.size(); i++) {
                call.setString(i + 1, parms.get(i).toString());
            }
            call.execute();
            call.close();
        } catch (SQLException e) {
            for (int i = 0; i < parms.size(); i++) {
                System.err.println("parm(" + i + "}: " + parms.get(i).toString());
                System.err.flush();
            }
        }
        notifyAll();
    }

    public void runCommand(String command) {
        try {
            CallableStatement call = con.prepareCall("{call " + command + ")");
            call.execute();
            call.close();
        } catch (SQLException e) {
            log("Error executing: " + command);
            e.printStackTrace();
        }
    }

    public void runSQLCommands(String thePLSQL) {
        try {
            BufferedReader theSQLBlock = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(thePLSQL.getBytes("iso-8859-1"))));
            try {
                String line = null;
                while ((line = theSQLBlock.readLine()) != null) {
                    if (line.length() > 4) {
                        log("exec " + line + ";");
                        runCommand(line);
                    }
                }
            } catch (IOException ioe) {
                log("IOException " + ioe);
            }
        } catch (UnsupportedEncodingException uee) {
            System.out.println("UnsupportedEncodingException " + uee);
        }
    }

    public void run() {
        int l = -10;
        Thread me = Thread.currentThread();
        log(me.getName() + " keyCount " + keys.size());
        if ((l = login()) != 0)
            log(me.getName() + " login returned " + 1);
        try {
            for (int i = 0; !interrupted() && i < keys.size(); i++) {
                me = Thread.currentThread();
                idNumber = keys.get(i).toString();
                log(me.getName() + " " + evaluator + "(" + idNumber + ")");
                System.out.flush();
                runParmCommand(evaluator + "('" + idNumber + "')");
                pool.keyCount--;
                if (pool.keyCount <= 0) {
                    log(me.getName() + " Total Processing Complete");
                    System.out.flush();
                    logout();
                    Thread.currentThread().interrupt();
                    return;
                }
                log(me.getName() + " pool.keyCount=" + pool.keyCount);
                Thread.sleep(5);
            }
        } catch (java.lang.InterruptedException ie) {
            System.out.println(ie);
            Thread.currentThread().interrupt();
            return;
        }
        log(me.getName() + " Thread Processing Complete");
        System.out.flush();
        logout();
    }
}
