
import java.util.Date;

class ruleEngineTask extends Thread {
    private ruleEngineDriver pool;
    public ruleEngineTask(ruleEngineDriver thePool) {
        pool = thePool;
    }
    public int log(String cmd) {
        java.util.Date now = null;
        now = new Date();
        System.out.println(now.toString() + " - " + cmd);
        System.out.flush();
        return 0;
    }
    public void run() {
        Runnable job = pool.getNext();
        try {
            log(pool.keyCount + " " + this.getName());
            job.run();
        } catch (Exception e) {
            System.err.println("Job exception: " + e);
            System.out.println("Job exception: " + e);
        }
    }
}