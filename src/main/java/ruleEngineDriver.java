
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.util.LinkedList;
import java.sql.*;
import java.math.*;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.util.*;

public class ruleEngineDriver {
    static String sourceTable;
    static String evaluator;
    static String userid;
    static String password;
    static String query;
    static String connectionMethodology;
    static int percentage = 0;
    private static Connection con;
    static int keyCount = 0;
    private LinkedList tasks = new LinkedList();

    public static int log(String cmd) {
        java.util.Date now = new java.util.Date();
        System.out.println(now.toString() + " - " + cmd);
        System.out.flush();
        return 0;
    }

    public ruleEngineDriver(int size) {
        for (int i = 0; i < size; i++) {
            Thread thread = new ruleEngineTask(this);
            thread.start();
        }
    }

    public static void loadConfig(String fileName) {
        Properties props = new Properties(System.getProperties());
        try {
            props.load(new BufferedInputStream(new FileInputStream(fileName)));
            System.setProperties(props);
        } catch (java.io.IOException ioe) {
        }
        userid = System.getProperty("userid");
        password = System.getProperty("password");
        query = System.getProperty("query");
        connectionMethodology = System.getProperty("connectionmethodology");
        if (System.getProperty("percentage") != null) {
            Integer tmppercentage = new Integer(System.getProperty("percentage"));
            percentage = tmppercentage.intValue();
        }
        sourceTable = System.getProperty("sourcetable");
        evaluator = System.getProperty("evaluator");
    }

    public static int login(String userid, String password) {
        String dbHostname = System.getProperty("dbhost");
        String dbSID =
                System.getProperty("dbsid");
        String dbPort = System.getProperty("dbport");
        String dbURL = System.getProperty("dburl");

        if (dbPort == null)
            dbPort = new String("1521");

        String connectString = new String("jdbc:oracle:thin:" + userid + "/" + password + "@" + dbHostname + ":" + dbPort + ":" + dbSID);
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        try {
            if (dbURL == null) con = DriverManager.getConnection(connectString);
            else
                con = DriverManager.getConnection(dbURL, userid, password);
        } catch (SQLException e) {
            if (dbURL == null)
                System.err.println("Error : " + connectString);
            else
                System.err.println("Error : " + dbURL);
            e.printStackTrace();
            return -2;
        }
        return 0;
    }

    public static void logout() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet executeSQL(String sql) {
        try {
            con.setAutoCommit(false);
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rset = stmt.executeQuery();
            return rset;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void runSQLCommands(String thePLSQL) {
        try {
            BufferedReader theSQLBlock = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(thePLSQL.getBytes("iso-8859-l"))));
            try {
                String line = null;
                while ((line = theSQLBlock.readLine()) != null) {
                    if (line.length() > 4) {
                        log("exec " + line + ";");
                        runCommand(line);
                    }
                }
            } catch (IOException ioe) {
                System.out.println("IOException " + ioe);
            }
        } catch (UnsupportedEncodingException uee) {
            System.out.println("UnsupportedEncodingException " + uee);
        }
    }

    public static void runCommand(String command) {
        try {
            CallableStatement call = con.prepareCall("{call " + command + "}");
            call.execute();
            call.close();
        } catch (SQLException e) {
            System.out.println("ERROR Executing: " + command);
            e.printStackTrace();
        }
    }

    public void run(Runnable task) {
        synchronized (tasks) {
            tasks.addLast(task);
            tasks.notifyAll();
        }
    }

    public Runnable getNext() {
        Runnable returnVal = null;
        synchronized (tasks) {
            while (tasks.isEmpty()) {
                try {
                    tasks.wait();
                } catch (InterruptedException ex) {
                    System.err.println("Interrupted: " + ex);
                }
            }
            returnVal = (Runnable) tasks.removeFirst();
        }
        return returnVal;
    }

    public static int currentRunningJobs(String id) {
        int jobs = 0;
        ResultSet rset;
        String jobquery = new String("SELECT count(1) FROM v$session WHERE client_info LIKE '" + id + "%' AND username = user");
        rset = executeSQL(jobquery);
        try {
            while (rset.next()) {
                Integer tmpPoolSize = new Integer(rset.getString(1));
                jobs = tmpPoolSize.intValue();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jobs;
    }

    public static String getPassword(String fileName) {
        StringBuffer dataResultBuffer = new StringBuffer("");
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = in.readLine()) != null) {
                dataResultBuffer.append(line);
            }
        } catch (IOException e) {
            System.out.println(e);
            System.err.println(e);
            System.exit(-1);
        }
        String data = new String(dataResultBuffer);
        return data;
    }

    public static void main(String args[]) {
        ResultSet rset;
        Vector keys = new Vector(4000);
        int threadpoolsize = 10;
        String configFile = null;
        if (args.length > 2)
            configFile = args[1];
        else if (args.length >= 1)
            configFile = args[0];
        else
            configFile = "engine.ruleEngineDriver.cfg";

        loadConfig(configFile);
        String passwordFile = System.getProperty("passwordFile");
        if (passwordFile == null)
            passwordFile = new String("/user/orapass/" + userid);

        password = getPassword(passwordFile);

        if (System.getProperty("threadpoolsize") != null) {
            Integer tmpPoolSize = new Integer(System.getProperty("threadpoolsize"));
            threadpoolsize = tmpPoolSize.intValue();
        }
        if (args.length < 1) {
            System.out.println("Syntax: java engine.ruleEngineDriver <UID> <PASSWORD>");
            return;
        }
        if (login(userid, password) < 0) {
            log("Failed to connect");
            return;
        }
        log("Connection Successful");
        log(query);

        runSQLCommands("dbms_application_info.set_client_info('engine.RuleTask " + sourceTable + "')\n");
        rset = executeSQL(query);
        try {
            while (rset.next()) {
                String idNumber = rset.getString(1);
                keys.addElement(idNumber);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        keyCount = keys.size();
        if (keyCount < 20)
            threadpoolsize = keyCount;
        int groups = threadpoolsize;
        int recordsPerGroup = (keyCount / threadpoolsize);

        log("keys to process: " + keyCount);
        log("groups(threads) to process: " + threadpoolsize);
        log("group size to process: " + recordsPerGroup);

        if (recordsPerGroup * threadpoolsize < keyCount) {
            log("Leftovers: " + (keyCount - (recordsPerGroup * threadpoolsize)));
            threadpoolsize++;
        }
        if (keyCount == 0) {
            log("NO KEYS TO PROCESS. exiting");
            return;
        }

        if (connectionMethodology.compareTo("percentageOfThreadPoolSize") == 0) {
            while (currentRunningJobs("engine.RuleTask Thread%" + sourceTable) > (threadpoolsize * percentage / 100)) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ie) {
                    System.out.println(ie);
                }
            }
        }
        ruleEngineDriver pool = new ruleEngineDriver(threadpoolsize);
        Vector newGroup = null;
        int groupId = 0;
        int keysAdded = 0;
        boolean newKeyGroup = true;
        for (int i = 0, n = keys.size(); i < n;i = i + 1){
            newGroup = new Vector();
            final int inner1 = i;
            if ((keyCount - keysAdded) > 0) {

                for (int eachV = i; eachV < (keyCount) && eachV < i + (recordsPerGroup); eachV++) {
                    String thisKey = null;
                    if (eachV == 1)
                        thisKey = keys.get(eachV).toString();
                    else {
                        if (eachV < keyCount - groups)
                            thisKey = keys.get(eachV + (groups + groupId)).toString();
                    }
                    newGroup.addElement(thisKey);
                    log("group " + groupId + " add key = " + (thisKey));
                    keysAdded++;
                }
                groupId++;
            }
            if (groupId == groups) {
                int leftOvers = (keyCount - keysAdded);
                for (int eachV = keysAdded, j = 0; j < leftOvers; eachV++, j++) {
                    newGroup.addElement(keys.get(eachV).toString());
                    keysAdded++;
                }
            }
            if (connectionMethodology.compareTo("maximizeToThreadPoolSize") == 0) {

                while (currentRunningJobs("engine.RuleTask Thread%" + sourceTable) > threadpoolsize) {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ie) {
                        System.out.println(ie);
                    }
                }
            }
            Runnable runner = new RuleTask(pool, evaluator, newGroup, new Integer(groupId).toString());
            pool.run(runner);
        }
        log("keysAdded:" + keysAdded);
        logout();
    }
}



